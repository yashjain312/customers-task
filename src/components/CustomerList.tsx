import React, { useState } from 'react';
import { List, ListItem, ListItemAvatar, ListItemText, ListItemSecondaryAction } from '@material-ui/core';
import { Avatar } from '@material-ui/core';
import { makeStyles } from '@material-ui/core';
import { deepOrange } from '@material-ui/core/colors';
import { Divider } from '@material-ui/core';
import DeleteIcon from '@material-ui/icons/Delete';
import { IconButton, Button } from '@material-ui/core'; 
import { Snackbar } from '@material-ui/core';
import MuiAlert, { AlertProps } from '@material-ui/lab/Alert';
import EditIcon from '@material-ui/icons/Edit';
import { Dialog, DialogActions, DialogContentText, DialogContent, DialogTitle } from '@material-ui/core';
import { TextField } from '@material-ui/core';
import SwipeableDrawer from '@material-ui/core/SwipeableDrawer';
import { Typography } from '@material-ui/core';
import { Card, CardContent, CardHeader } from '@material-ui/core'; 
import { Grid } from '@material-ui/core';

function Alert(props: AlertProps) {
    return <MuiAlert elevation={6} variant="filled" {...props} />;
}

interface Props {
    items: Customer[];
    deleteItem(key: number | string): void;
    updateItem(item: Customer, key: number | string): void;
};

interface Customer {
    name: string;
    email: string;
    phone: string;
    key: number | string;
}

const useStyles = makeStyles((theme) => ({
    orange: {
        color: theme.palette.getContrastText(deepOrange[500]),
        backgroundColor: deepOrange[500],
    },
    inputStyle: {
        marginTop: "10px"
    },
    typographyStyle: {
        color: "grey",
        width: "130px",
        marginLeft: "0"
    },
    width: {
        width: "800px"
    },
    list: {
        marginTop: "50px"
    },
    header: {
        color: "grey",
        textTransform: "uppercase",
        marginLeft: "10px"
    },
    grid: {
        marginTop: "20px"
    }
}));


const ListCustomers: React.FC<Props> = ({ items, deleteItem, updateItem }) => {
    const classes = useStyles();

    const [openSnackbar, setOpenSnackbar] = useState(false);
    const [openEditButton, setOpenEditButton] = useState(false);
    const [openDrawer, setOpenDrawer] = useState(false); 

    const handleCloseSnackbar = () => {
        setOpenSnackbar(false);
    }

    const handleOpenSnackbar = () => {
        setOpenSnackbar(true);
    }

    const handleCloseEditButton = () => {
        setOpenEditButton(false);
    }

    const handleOpenEditButton = () => {
        setOpenEditButton(true);
    }

    const handleOpenDrawer = () => {
        setOpenDrawer(true);
    }

    const handleCloseDrawer = () => {
        setOpenDrawer(false);
    }


    const handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        const { name, value } = e.target;
        setSelectedItem({...selectedItem, [name]: value});
      }

    const [selectedItem, setSelectedItem] = useState<Customer>({name: "", email: "", phone: "", key: ""});

    const listItems = items.map((item) => {
        return (
            <div key={item.key}>
                <ListItem onClick={() => {
                    handleOpenDrawer();
                    setSelectedItem({name: item.name, email: item.email, phone: item.phone, key: item.key});
                }}>
                    <ListItemAvatar>
                        <Avatar variant="circle" className={classes.orange}>{item.name.charAt(1).toUpperCase()}</Avatar>
                    </ListItemAvatar>
                    <ListItemText primary={item.name} secondary={item.email}/>
                    <ListItemSecondaryAction>
                        <IconButton onClick={() => {
                            handleOpenEditButton();
                            setSelectedItem({name: item.name, email: item.email, phone: item.phone, key: item.key});
                        }}>
                            <EditIcon/>
                        </IconButton>
                        <IconButton onClick={() => {
                            deleteItem(item.key);
                            handleOpenSnackbar();
                        }}>
                            <DeleteIcon/>
                        </IconButton>
                    </ListItemSecondaryAction>
                </ListItem>
                <Divider/>
            </div>
        );
    });

    const drawerDisplay = (
        <div className={classes.width}>
            <List className={classes.list}>
                <ListItem>
                    <ListItemAvatar>
                        <Avatar variant="circle" className={classes.orange}>{selectedItem.name.charAt(1).toUpperCase()}</Avatar>
                    </ListItemAvatar>
                    <ListItemText primary={selectedItem.name} secondary={selectedItem.email}/>
                </ListItem>
            </List>
            <Grid container className={classes.grid}>
                <Grid item sm={1}></Grid>
                <Grid item sm={10}>
                    <Card>
                        <CardHeader 
                            title={<h3 className={classes.header}>Details</h3>}
                        />
                        <CardContent>
                            <List>
                                <ListItem>
                                    <Typography className={classes.typographyStyle}>Name:</Typography>
                                    <Typography>{selectedItem.name}</Typography>
                                </ListItem>
                                <ListItem>
                                    <Typography className={classes.typographyStyle}>Email:</Typography>
                                    <Typography>{selectedItem.email}</Typography>
                                </ListItem>
                                <ListItem>
                                    <Typography className={classes.typographyStyle}>Phone Number:</Typography>
                                    <Typography>{selectedItem.phone}</Typography>
                                </ListItem>
                            </List>
                        </CardContent>
                    </Card>
                </Grid>
                <Grid item sm={1}></Grid>
            </Grid>
            <Grid container className={classes.grid}>
                <Grid item sm={1}></Grid>
                <Grid item sm={10}>
                    <Card>
                        <CardHeader 
                            title={<h3 className={classes.header}>Special Dates</h3>}
                        />
                        <CardContent>
                            <List>
                                <ListItem>
                                    <Typography className={classes.typographyStyle}>Birth Date:</Typography>
                                </ListItem>
                            </List>
                        </CardContent>
                    </Card>
                </Grid>
                <Grid item sm={1}></Grid>
            </Grid>
        </div>
    );

    return (
        <div>
            <List>
                {listItems}
            </List>  
            <Snackbar open={openSnackbar} onClose={handleCloseSnackbar} autoHideDuration={6000}>
                <Alert onClose={handleCloseSnackbar} severity="info">
                    Customer Deleted Successfully!
                </Alert>
            </Snackbar>
            <Dialog open={openEditButton} onClose={handleCloseEditButton} aria-labelledby="alert-dialog-title" aria-describedby="alert-dialog-description">
                <DialogTitle id="alert-dialog-title">Update Customer</DialogTitle>
                <DialogContent>
                    <DialogContentText id="alert-dialog-description">
                        <form id="customer_form">
                            <TextField
                                variant="standard"
                                label={"Name"} 
                                name={"name"}
                                value={selectedItem.name}
                                fullWidth
                                className={classes.inputStyle}
                                onChange={handleChange}
                            />
                            <TextField
                                variant="standard"
                                label={"Email"}
                                name={"email"}
                                value={selectedItem.email}
                                fullWidth
                                className={classes.inputStyle} 
                                onChange={handleChange}
                            />
                            <TextField
                                variant="standard"
                                label={"Phone"} 
                                name={"phone"}
                                value={selectedItem.phone}
                                fullWidth
                                className={classes.inputStyle}
                                onChange={handleChange}
                            />
                            <DialogActions>
                                <Button onClick={handleCloseEditButton}>Cancel</Button>
                                <Button onClick={() => {
                                    handleCloseEditButton();
                                    updateItem(selectedItem, selectedItem.key);
                                }} color="primary" variant="contained" form="customer_form">Save</Button>
                            </DialogActions>
                        </form>
                    </DialogContentText>
                </DialogContent>
            </Dialog>
            <SwipeableDrawer open={openDrawer} onClose={handleCloseDrawer} onOpen={handleOpenDrawer} anchor="right">
                {drawerDisplay}
            </SwipeableDrawer>
        </div>      
    );
};

export default ListCustomers;
