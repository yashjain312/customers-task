import React, { useState } from 'react';
import './App.css';
import ListCustomers from './components/CustomerList';
import { Grid } from '@material-ui/core';
import { Typography } from '@material-ui/core';
import { makeStyles } from '@material-ui/core';
import { Button } from '@material-ui/core';
import AddCircleRoundedIcon from '@material-ui/icons/AddCircleRounded';
import { Dialog, DialogActions, DialogContentText, DialogContent, DialogTitle } from '@material-ui/core';
import { TextField } from '@material-ui/core';

const useStyles = makeStyles({
  typographyStyle: {
      textAlign: "left",
      fontSize: "25px",
      fontWeight: "bold"
  },
  root: {
      padding: "20px"
  },
  inputStyle: {
    marginTop: "10px"
  },
});


interface Props {
  
};

interface Customer {
  name: string;
  email: string;
  phone: string;
  key: number | string;
}

const App: React.FC<Props> = () => {

  const [open, setOpen] = useState(false);
  const [items, setItems] = useState<Customer[]>([
    {
      name: "Yash Jain",
      email: "yash@appointy.com",
      phone: "7016602035",
      key: Date.now()
    },
    {
      name: "Suresh Jain",
      email: "suresh@appointy.com",
      phone: "7016602025",
      key: Date.now() + 30
    },
    {
      name: "Ramesh Jain",
      email: "ramesh@appointy.com",
      phone: "7016602099",
      key: Date.now() + 60
    },
  ]);
  const [currentItem, setCurrentItem] = useState<Customer>({name: "", email: "", phone: "", key: ""});

  const addItem = () => {
    const newItem = currentItem;
    console.log(newItem);
    newItem.key = Date.now();
    if(newItem) {
      if(newItem.name !== " " && newItem.email !== " " && newItem.phone !== " ") {
        setItems([...items, newItem]);
        setCurrentItem({name: " ", email: " ", phone: " ", key: " "});
      }
    }
  }

  const handleClick = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    const { name, value } = e.target;
    setCurrentItem({...currentItem, [name]: value});
  }

  const deleteItem = (key: number | string): void => {
    const filteredItems = items.filter(item => {
      return item.key !== key;
    });
    setItems([...filteredItems]);
  }

  const updateItem = (item: Customer, key: number | string): void => {
    const allItems = items;
    allItems.map(i => {
      if(i.key === key) {
        i.name = item.name;
        i.email = item.email;
        i.phone = item.phone;
      }
    });
    setItems([...allItems]);
  }

  const classes = useStyles();

  return (
    <div className="App">
      <Grid container className={classes.root}>
          <Grid item xs={12} sm={8}>
              <Typography className={classes.typographyStyle}>Customers</Typography>
          </Grid>
          <Grid item xs={12} sm={4}>
              <Button variant="outlined" startIcon={<AddCircleRoundedIcon/>} onClick={handleClick} color="primary">Add Customer</Button>
              <Dialog open={open} onClose={handleClose} aria-labelledby="alert-dialog-title" aria-describedby="alert-dialog-description">
                  <DialogTitle id="alert-dialog-title">New Customer</DialogTitle>
                  <DialogContent>
                      <DialogContentText id="alert-dialog-description">
                          <form id="customer_form">
                              <TextField
                                  variant="standard"
                                  label={"Name"} 
                                  name={"name"}
                                  value={currentItem.name}
                                  fullWidth
                                  className={classes.inputStyle}
                                  onChange={handleChange}
                              />
                              <TextField
                                  variant="standard"
                                  label={"Email"}
                                  name={"email"}
                                  value={currentItem.email}
                                  fullWidth
                                  className={classes.inputStyle} 
                                  onChange={handleChange}
                              />
                              <TextField
                                  variant="standard"
                                  label={"Phone"} 
                                  name={"phone"}
                                  value={currentItem.phone}
                                  fullWidth
                                  className={classes.inputStyle}
                                  onChange={handleChange}
                              />
                              <DialogActions>
                                  <Button onClick={handleClose}>Cancel</Button>
                                  <Button onClick={() => {
                                    handleClose();
                                    addItem();
                                  }} color="primary" variant="contained" form="customer_form">Confirm</Button>
                              </DialogActions>
                          </form>
                      </DialogContentText>
                  </DialogContent>
              </Dialog>
          </Grid>
      </Grid>
      {items && <ListCustomers items={items} deleteItem={deleteItem} updateItem={updateItem}/>}
    </div>
  );
}

export default App;
